# Google Maps Polyline to Lat/Lon converter
# usage: polyline_geocoord.py <polyline_str>

import os
import sys

polyline_str = sys.argv[1]

index, lat, lng = 0, 0, 0
coordinates = []
changes = {'latitude': 0, 'longitude': 0}

# Coordinates have variable length when encoded, so just keep
# track of whether we've hit the end of the string. In each
# while loop iteration, a single coordinate is decoded.
while index < len(polyline_str):
	# Gather lat/lon changes, store them in a dictionary to apply them later
	for unit in ['latitude', 'longitude']: 
		shift, result = 0, 0

		while True:
			byte = ord(polyline_str[index]) - 63
			index+=1
			result |= (byte & 0x1f) << shift
			shift += 5
			if not byte >= 0x20:
				break

		if (result & 1):
			changes[unit] = ~(result >> 1)
		else:
			changes[unit] = (result >> 1)

	lat += changes['latitude']
	lng += changes['longitude']

	coordinates.append((lat / 100000.0, lng / 100000.0))

print coordinates