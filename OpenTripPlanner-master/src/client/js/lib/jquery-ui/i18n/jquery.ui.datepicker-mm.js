/* Myanmar initialisation for the jQuery UI date picker plugin. */
/* Zawyi font format
/* Written by Leonard Aye (len.aye@nandawon.com). */
jQuery(function($){
	$.datepicker.regional['mm'] = {
		closeText: 'ပိတ္ျမည္',
		prevText: '&#x3C;ေနာက္သို႕',
		nextText: 'ေရွ႕သို႕&#x3E;',
		currentText: 'ဒီေန႕',
		monthNames: ['January','February','March','April','May','June',
		'July','August','September','October','November','December'],
		monthNamesShort: ['Jan','Feb','Mar','Apr','May','Jun',
		'Jul','Aug','Sep','Oct','Nov','Dec'],
		dayNames: ['Sunday','Monday','Tuesday','Wednesday','Thursday','Frday','Saturday'],
		dayNamesShort: ['Su','Mo','Tu','We','Th','Fr','Sa'],
		dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
		weekHeader: 'KW',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['mm']);
});
