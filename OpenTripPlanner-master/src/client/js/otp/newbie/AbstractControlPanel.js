L.Control.AbstractControlPanel = L.Control.extend({
    options: {
        className: 'leaflet-control-panel',
        collapsedStateTitle: 'Collapsed',
        collapsedIcon: 'http://cdn.leafletjs.com/leaflet-0.7.7/images/layers.png'
    },

    initialize: function (options) {
        L.setOptions(this, options);

        this._lastZIndex = 0;
        this._handlingClick = false;
    },

    onAdd: function (map) {
        this._initLayout();
        this._createUIElements();
        this._update();

        //this.callHandlers();

        return this._container;
    },

    /*onRemove: function (map) {
        //
    },*/

    _initLayout: function () {
        this._className = this.options.className;
        var container = this._container = L.DomUtil.create('div', this._className);

        //Makes this work on IE10 Touch devices by stopping it from firing a mouseout event when the touch is released
        container.setAttribute('aria-haspopup', true);

        if (!L.Browser.touch) {
            L.DomEvent
                .disableClickPropagation(container)
                .disableScrollPropagation(container);
        } else {
            L.DomEvent.on(container, 'click', L.DomEvent.stopPropagation);
        }

        var form = this._form = L.DomUtil.create('form', this._className + '-list');

        if (this.options.collapsed) {
            if (!L.Browser.android) {
                L.DomEvent
                    .on(container, 'mouseover', this._expand, this)
                    .on(container, 'mouseout', this._collapse, this);
            }
            var link = this._collapsedLink = L.DomUtil.create('a', this._className + '-toggle', container);
            link.href = '#';
            link.title = this.options.collapsedStateTitle;
            if (this.options.collapsedIcon) {
                link.style.backgroundImage = 'url(' + this.options.collapsedIcon + ')';
            }

            if (L.Browser.touch) {
                L.DomEvent
                    .on(link, 'click', L.DomEvent.stop)
                    .on(link, 'click', this._expand, this);
            }
            else {
                L.DomEvent.on(link, 'focus', this._expand, this);
            }
            //Work around for Firefox android issue https://github.com/Leaflet/Leaflet/issues/2033
            L.DomEvent.on(form, 'click', function () {
                setTimeout(L.bind(this._onInputClick, this), 0);
            }, this);

            this._map.on('click', this._collapse, this);
            // TODO keyboard accessibility
        } else {
            this._expand();
        }
        
        //var list = L.DomUtil.create('div', this._className + '-list');
        //container.appendChild(list);

        container.appendChild(form);
    },

    _update: function () {
        if (!this._container) {
            return;
        }
    },

    _expand: function () {
        L.DomUtil.addClass(this._container, this._className + '-expanded');
    },

    _collapse: function () {
        this._container.className = this._container.className.replace(' ' + this._className + '-expanded', '');
    },

    _createUIElements: function () {
        alert('Should be implemented!');
    },

    // Here is a copy of
    // http://stackoverflow.com/questions/118693/how-do-you-dynamically-create-a-radio-button-in-javascript-that-works-in-all-bro/119079#119079            
    _createRadioElement : function(name, checked, className) {
        var radioHtml = '<input type="radio" class="' + className + '" name="' + name + '"';
        if ( checked ) {
            radioHtml += ' checked="checked"';
        }
        radioHtml += '/>';

        var radioFragment = document.createElement('div');
        radioFragment.innerHTML = radioHtml;

        return radioFragment.firstChild;
    },

    _createInputElement : function(attrs) {
        var inputHtmlArray = ['<input'];

        attrs.forEach( function( item, arrIndex, arr ) {
            this.push( item.id + '="' + item.value + '"' );
        }, inputHtmlArray);        

        inputHtmlArray.push( '/>' );
        var inputHtml = inputHtmlArray.join(' ');

        var inputFragment = document.createElement('div');
        inputFragment.innerHTML = inputHtml;

        return inputFragment.firstChild;
    }
});