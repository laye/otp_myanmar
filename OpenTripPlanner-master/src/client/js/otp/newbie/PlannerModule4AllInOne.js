/**
 * 
 */

otp.namespace("otp.newbie");


otp.newbie.PlannerModule4AllInOne =
    otp.Class(otp.modules.planner.PlannerModule, {

    //TRANSLATORS: module name
    moduleName  : _tr("All-In_One Trip Planner"),

    itinWidget  : null,

    showIntermediateStops : false,

    stopsWidget: false,

    routeData : null,

    initialize : function(webapp, id, options) {
        otp.modules.planner.PlannerModule.prototype.initialize.apply(this, arguments);
    },

    activate : function() {
        if(this.activated) return;
        otp.modules.planner.PlannerModule.prototype.activate.apply(this);

        // set up options widget

        var optionsWidgetConfig = {
                //TRANSLATORS: widget name
                title : _tr("Trip Options"),
                closeable : true,
                persistOnClose: true,
                'position' : 'topleft'
        };

        if(typeof this.tripOptionsWidgetCssClass !== 'undefined') {
            console.log("set tripOptionsWidgetCssClass: " + this.tripOptionsWidgetCssClass);
            optionsWidgetConfig['cssClass'] = this.tripOptionsWidgetCssClass;
        }

        this.optionsWidget = new otp.newbie.TripWidgetAllInOne(
            'otp-'+this.id+'-optionsWidget', this, optionsWidgetConfig, this.webapp.geocoders);

        /*if(this.webapp.geocoders && this.webapp.geocoders.length > 0) {
            this.optionsWidget.addControl("locations", new otp.widgets.tripoptions.LocationsSelector(this.optionsWidget, this.webapp.geocoders), true);
            this.optionsWidget.addVerticalSpace(12, true);
        }

        this.optionsWidget.addControl("time", new otp.widgets.tripoptions.TimeSelector(this.optionsWidget), true);
        this.optionsWidget.addVerticalSpace(12, true);


        var modeSelector = new otp.widgets.tripoptions.ModeSelector(this.optionsWidget);
        this.optionsWidget.addControl("mode", modeSelector, true);*/

        /*modeSelector.addModeControl(new otp.widgets.tripoptions.MaxWalkSelector(this.optionsWidget));
        modeSelector.addModeControl(new otp.widgets.tripoptions.MaxBikeSelector(this.optionsWidget));
        modeSelector.addModeControl(new otp.widgets.tripoptions.BikeTriangle(this.optionsWidget));
        modeSelector.addModeControl(new otp.widgets.tripoptions.PreferredRoutes(this.optionsWidget));
        modeSelector.addModeControl(new otp.widgets.tripoptions.BannedRoutes(this.optionsWidget));

        // hide wheelchair selector only if config explicitly set to false; show by default if setting is not present
        if(otp.config.showWheelchairOption === undefined || otp.config.showWheelchairOption === true) {
          modeSelector.addModeControl(new otp.widgets.tripoptions.WheelChairSelector(this.optionsWidget));
        }
        modeSelector.refreshModeControls();

        this.optionsWidget.addSeparator();
        this.optionsWidget.addControl("submit", new otp.widgets.tripoptions.Submit(this.optionsWidget));

        this.optionsWidget.applyQueryParams(this.defaultQueryParams);*/

        // add stops layer
        this.stopsLayer = new otp.newbie.IntermediateStopsLayer(this, this.options);
    },

    routesLoaded : function() {
        // set trip / stop viewer widgets

        this.tripViewerWidget = new otp.widgets.transit.TripViewerWidget("otp-"+this.id+"-tripViewerWidget", this);
        this.tripViewerWidget.center();

        this.stopViewerWidget = new otp.widgets.transit.StopViewerWidget("otp-"+this.id+"-stopViewerWidget", this);
        this.stopViewerWidget.center();

    },

    getExtendedQueryParams : function() {
        return { showIntermediateStops : this.showIntermediateStops };
    },

    processPlan : function(tripPlan, restoring) {
    	this.optionsWidget.updateItineraries(tripPlan.itineraries, tripPlan.queryParams, this.restoredItinIndex);
        /*if(this.itinWidget == null) {
            this.itinWidget = new otp.widgets.ItinerariesWidget(this.id+"-itinWidget", this);
        }
        if(restoring && this.restoredItinIndex) {
            this.itinWidget.show();
            this.itinWidget.updateItineraries(tripPlan.itineraries, tripPlan.queryParams, this.restoredItinIndex);
            this.restoredItinIndex = null;
        } else  {
            this.itinWidget.show();
            this.itinWidget.updateItineraries(tripPlan.itineraries, tripPlan.queryParams);
        }*/

        /*if(restoring) {
            this.optionsWidget.restorePlan(tripPlan);
        }*/
        this.drawItinerary(tripPlan.itineraries[0]);
        this.stopsLayer.setSelectedItinerary(tripPlan.itineraries[0]);
    },

    restoreTrip : function(queryParams) {
        this.optionsWidget.applyQueryParams(queryParams);
        otp.modules.planner.PlannerModule.prototype.restoreTrip.apply(this, arguments);
    },

    clearTrip : function() {
        otp.modules.planner.PlannerModule.prototype.clearTrip.apply(this);
        if(this.itinWidget !== null) {
            this.itinWidget.close();
            this.itinWidget.clear();
            //TRANSLATORS: Widget title
            this.itinWidget.setTitle(_tr("Itineraries"));
        }
    },

    CLASS_NAME : "otp.newbie.PlannerModule4AllInOne"
});
