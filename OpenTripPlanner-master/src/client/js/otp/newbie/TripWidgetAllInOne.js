/**
 * 
 */
otp.namespace("otp.newbie");

otp.newbie.TripWidgetAllInOne =
    otp.Class(otp.widgets.Widget, {

    //planTripCallback : null,
    controls : null,
    module : null,

    scrollPanel : null,

    autoPlan : false,
    
    //LocationSelector
    geocoders    :  null,
    activeIndex  :  0,

    initialize : function(id, module, options, geocoders) {

    	options = L.extend({},
        	{
			    'position' : 'bottomleft',
			    'delimeter' : 'br',
			    'whenList' : [
			    	{ 'value' : 'leave_now', 'description' : _tr("Now"), 'selected' : true }, //'Leave Now'
			        { 'value' : 'depart_by', 'description' : _tr("Depart by") },			        
			        { 'value' : 'arrive_by', 'description' : _tr("Arrive by") }
			    ],
			    'onBtnGoClick' : L.bind(function() {
			    	switch ($( "#when" ).val()) {
			    		case 'leave_now' :
			    			var justNow = (new Date()).toISOString(),
			    				separatorPos = justNow.indexOf('T');
			    			this.tripWidget.inputChanged({
			                    //date : justNow.substring(0, separatorPos),
			                    date : moment().format(otp.config.locale.time.date_format),
			                    time : moment().format(otp.config.locale.time.time_format)
			                });
			    			break;
			    		case 'depart_by' :
			        	case 'arrive_by' :
			        	default :
			        		break;
			        }
			    	var this_ = this;
		            //this_.tripWidget.pushSettingsToModule();
		            if(typeof this_.tripWidget.module.userPlanTripStart == 'function') this_.tripWidget.module.userPlanTripStart();
		            this_.tripWidget.module.planTripFunction.apply(this_.tripWidget.module);
		        }, this),
		        //'onClickItinerary' : { 'handler' : this._onClickItinerary, 'context' : this }
		        'onWhenSelectChanged' : L.bind(function() {
		        	this.tripWidget.module.arriveBy = ( 'arrive_by' === $( "#when" ).val());
		        }, this),
		        'onDatePickerChanged' : L.bind(function(e) {
		        	this.tripWidget.inputChanged({
	                    date : moment(e.date).format(otp.config.locale.time.date_format)
		                //time : moment(dt).format(otp.config.locale.time.time_format)
	                });
		        }, this),
                'onTimePickerChanged' : L.bind(function(e) {
                    this.tripWidget.inputChanged({
                        //time : moment(dt).format(otp.config.locale.time.time_format)
                        time : e.time.value + e.time.meridian
                    });
                }, this),
		        'onReverseButtonClick' : L.bind(function() {
		        	var module = this.tripWidget.module;
		            var startLatLng = module.startLatLng, startName = module.startName;
		            var endLatLng = module.endLatLng, endName = module.endName;
		            module.clearTrip();
		            module.setStartPoint(endLatLng, false, endName);
		            module.setEndPoint(startLatLng, false, startName);
		            this.tripWidget.inputChanged();
		        }, this),
                'onClearButtonClick' : L.bind(function(e) {
                    this.module.clearTrip();
                    $("#locSelector-start").val('');
                    $("#locSelector-stop").val('');
                }, this)
                
			}, 
			options
        );
        //TRANSLATORS: Widget title
        if(!_.has(options, 'title')) options['title'] = _tr("Travel Options");
        if(!_.has(options, 'cssClass')) options['cssClass'] = 'otp-allinoneTripPlanner';
        otp.widgets.Widget.prototype.initialize.call(this, id, module, options);

        //this.mainDiv.addClass('otp-tripOptionsWidget');

        //this.planTripCallback = planTripCallback;
        this.module = module;

        this.controls = {};
        
        this.tripWidget = this;
        
        this._tripPlanner = new L.Control.TripPlanner(options);
        this.module.webapp.map.lmap.addControl(this._tripPlanner);
        
        this.geocoders = geocoders;
        this.initializeLocationsSelector(this, this.geocoders);
    },

    addControl : function(id, control, scrollable) {
    	console.log('TripPlannerAllInOne.addControl');
    },

    initScrollPanel : function() {
    	console.log('TripPlannerAllInOne.initScrollPanel');
    },

    addSeparator : function(scrollable) {
    	console.log('TripPlannerAllInOne.addSeparator');
    },

    addVerticalSpace : function(pixels, scrollable) {
    	console.log('TripPlannerAllInOne.addVerticalSpace');
    },

    restorePlan : function(data) {
	    if(data == null) return;

	    for(var id in this.controls) {
            this.controls[id].restorePlan(data);
        }
    },

    applyQueryParams : function(queryParams) {
        this.restorePlan({ queryParams : queryParams });
    },

    restoreDefaults : function(useCurrentTime) {
        var params = _.clone(this.module.defaultQueryParams);
        if(useCurrentTime) {
            params['date'] = moment().format(otp.config.locale.time.date_format);
            params['time'] = moment().format(otp.config.locale.time.time_format);
        }
        this.applyQueryParams(params);
    },

    newItinerary : function(itin) {
        for(var id in this.controls) {
            this.controls[id].newItinerary(itin);
        }
    },

    inputChanged : function(params) {
        if(params) _.extend(this.module, params);
        if(this.autoPlan) {
            this.module.planTrip();
        }
    },

    //LocationsSelector
    initializeLocationsSelector : function(tripWidget, geocoders) {
        console.log("init loc");
        this.geocoders = geocoders;

        otp.widgets.tripoptions.TripOptionsWidgetControl.prototype.initialize.apply(this, arguments);
        this.id = tripWidget.id+"-locSelector";

        ich['otp-tripOptions-locations']({
            widgetId : this.id,
            showGeocoders : (this.geocoders && this.geocoders.length > 1),
            geocoders : this.geocoders,
            //TODO: Maybe change to Start and Destination
            start: pgettext('template', "Start"),
            end: _tr("End"),
            geocoder: _tr("Geocoder")
        });//.appendTo(this.$());

        //TODO
        this.tripWidget.module.on("startChanged", $.proxy(function(latlng, name) {
            $("#locSelector-start").val(name || '(' + latlng.lat.toFixed(5) + ', ' + latlng.lng.toFixed(5) + ')');
        }, this));

        //TODO
        this.tripWidget.module.on("endChanged", $.proxy(function(latlng, name) {
            $("#locSelector-stop").val(name || '(' + latlng.lat.toFixed(5) + ', ' + latlng.lng.toFixed(5) + ')');
        }, this));
        
        this.initUIElements();
    },

    initUIElements : function() {
    	this.startInput = this.initInput($("#locSelector-start"), this.tripWidget.module.setStartPoint);
        this.endInput = this.initInput($("#locSelector-stop"), this.tripWidget.module.setEndPoint);
    },

    initInput : function(input, setterFunction) {
        var this_ = this;
        input.autocomplete({
            delay: 500, // 500ms between requests.
            source: function(request, response) {
                this_.geocoders[this_.activeIndex].geocode(request.term, function(results) {
                    console.log("got results "+results.length);
                    response.call(this, _.pluck(results, 'description'));
                    input.data("results", this_.getResultLookup(results));
                });
            },
            select: function(event, ui) {
                var result = input.data("results")[ui.item.value];
                var latlng = new L.LatLng(result.lat, result.lng);
                this_.tripWidget.module.webapp.map.lmap.panTo(latlng);
                setterFunction.call(this_.tripWidget.module, latlng, false, result.description);
                this_.tripWidget.inputChanged();
            },
        })
        .dblclick(function() {
            $(this).select();
        });
        return input;
    },
    
    getResultLookup : function(results) {
        var resultLookup = {};
        for(var i=0; i<results.length; i++) {
            resultLookup[results[i].description] = results[i];
        }
        /*var resultLookup = [];
        for(var i=0; i<results.length; i++) {
            resultLookup.push(label : results[i].description, value : results[i].id);
        }*/
        return resultLookup;
    },
    
    updateItineraries : function(itineraries, queryParams, itinIndex) {
    	console.log('updateItineraries:', itineraries, queryParams, itinIndex);
    	this._tripPlanner.selectItinerariesMode();
    	$("ol.journey-results").empty();
    	this.itineraries = itineraries;
    	for(var i=0; i<this.itineraries.length; i++) {
            var itin = this.itineraries[i];
            this.renderItinerary(itin, i);
        }
        this.activeIndex = parseInt(itinIndex) || 0;
        //$( "ol.journey-results > li" )[this.activeIndex].addClass('journey-selected');
        $( "ol.journey-results > li" ).eq(this.activeIndex).addClass('journey-selected');
    },

    renderItinerary : function(itin, index, alerts) {
    	var legSeqId = 'legSeqId' + index,
    		this_ = this;
    	ich['otp-journeyresults-result']({
    		'duration_text': _tr("Time"),
    		'duration': itin.getDurationStr(),
    		'legSeqId': legSeqId
    	})
    	.appendTo($('ol.journey-results'))
    	.data('itin', itin)
        .data('index', index)
        .click(function(evt) {
            var itin = $(this).data('itin');
            this_.module.drawItinerary(itin);
            this_.module.stopsLayer.setSelectedItinerary(itin);
            this_.activeIndex = $(this).data('index');
            $( "ol.journey-results > li" ).each( function(index, element) { $(element).removeClass('journey-selected'); });
            $(this).addClass('journey-selected');
            this_.renderItineraryDetails(this, itin);
        });
    	var legSeq = $('#' + legSeqId),
    		serviceDate1stLeg = Number( itin.itinData.legs[0].serviceDate );
    	for(var l=0; l<itin.itinData.legs.length; l++) {
    		var leg = itin.itinData.legs[l],
    		//var startTimeStr = otp.util.Time.formatItinTime(itin.getStartTime(), otp.config.locale.time.time_format);
    		//var endTimeStr = otp.util.Time.formatItinTime(itin.getEndTime(), otp.config.locale.time.time_format);
    			startTimeStr = otp.util.Time.formatItinTime(leg.startTime, otp.config.locale.time.time_format),
    			endTimeStr = otp.util.Time.formatItinTime(leg.endTime, otp.config.locale.time.time_format),
    			//itinDuration = otp.util.Time.calcDuration(itin.itinData.legs[0].startTime, itin.itinData.legs[itin.itinData.legs.length - 1].endTime);
    			itinDuration = Math.floor(otp.util.Time.calcDuration(itin.itinData.legs[0].startTime, leg.endTime));
    		ich['otp-journeyresults-leg']({
        		'startTime' : startTimeStr,
        		'endTime' : endTimeStr,
        		'endTimeAdditional' : 0 == itinDuration ? '' : '(+' + itinDuration + ')',
        		'routeShortName' : leg.routeShortName
        	}).appendTo(legSeq);
    	}
    	//$(itinHtml).appendTo($('ol.journey-results'));
    },
    
    renderItineraryDetails : function (itinDiv, itin) {
    	var this_ = this;
    	// Tidy up the previous details
    	//$( "ol.journey-results > li > .journey-details-element" ).each(
    	$( ".journey-details-element" ).each(
    		//.parent().get( 0 ).remove()
    		function(index, element) { $(element).parent().remove(); }
    	);
    	
    	var startTimeStr = otp.util.Time.formatItinTime(itin.getStartTime(), otp.config.locale.time.time_format),
			endTimeStr = otp.util.Time.formatItinTime(itin.getEndTime(), otp.config.locale.time.time_format),
			//itinDuration = Number( itin.itinData.legs[itin.itinData.legs.length - 1].serviceDate ) - Number( itin.itinData.legs[0].serviceDate );
			itinDuration = Math.floor(otp.util.Time.calcDuration(itin.itinData.legs[0].startTime, itin.itinData.legs[itin.itinData.legs.length - 1].endTime));
    	ich['otp-journeyresults-details-element']({
    		'startTime' : startTimeStr,
    		'endTime' : endTimeStr,
    		'timeAdditional' : 0 == itinDuration ? '' : '(+' + itinDuration + ')'
    	}).insertAfter(itinDiv);
    	
    	var insertionGoal = $( ".journey-details-element" ).get(0); 
    	
    	var	durationLbl = _tr("Time"), 
    		towardsLbl = _tr("Towards"),
    		stopsLbl = _tr("Stops");

    	for(var l=0; l<itin.itinData.legs.length; l++) {
    		var leg = itin.itinData.legs[l],
    			startTimeStr = otp.util.Time.formatItinTime(leg.startTime, otp.config.locale.time.time_format),
    			endTimeStr = otp.util.Time.formatItinTime(leg.endTime, otp.config.locale.time.time_format),
    			duration1st_currentLeg = Math.floor(otp.util.Time.calcDuration(itin.itinData.legs[0].startTime, leg.startTime)),
    			legArrivalTimeData = {};
    		if (l<itin.itinData.legs.length - 1) {
    			legArrivalTimeData = {
    				showLegArrivalTime : true,
    				arriveText : _tr("Arrive"),
    				legStopTime : endTimeStr,
    				legNextDay : this.composeNextDaySentence(Math.floor(otp.util.Time.calcDuration(itin.itinData.legs[0].startTime, leg.endTime)), null),
                    transferDurationLabel : _tr("Transfer duration"),
                    transferDurationValue : otp.util.Time.secsToHrMin( (itin.itinData.legs[l+1].startTime - leg.endTime)/1000 )
    			};
    		}

    		var details = ich['otp-journeyresults-details-stop']($.extend({
    			'stopName' : leg.from.name,
    			'stopTime' : startTimeStr,
    			'nextDay' : this.composeNextDaySentence(duration1st_currentLeg, null),
    			'showLeg' : true, //l < itin.itinData.legs.length - 1,
        		'routeShortName' : leg.routeShortName,
                'agencyName' : leg.agencyName,
        		'duration_text' : durationLbl,
        		'duration' : otp.util.Time.secsToHrMin( leg.duration ),
        		'towards_text' : towardsLbl,
    			'towardsStop' : leg.headsign,
    			'stops_text' : stopsLbl,
    			'stopsAmount' : leg.to.stopIndex - leg.from.stopIndex - 1,
    			'tripId' : leg.tripId
        	}, legArrivalTimeData));
    		details.find('.journey-details-leg-info-last-row').data('tripId', leg.tripId).data('leg', leg);
    		details.find('.triangle-down-img').click(function(evt) {
    			var tripId = $(this).parent().data('tripId'),
    				leg = $(this).parent().data('leg'),
    				context = { 
						'tripId' : tripId,
						'leg' : leg,
						//'isFirstTrip': 0 == legI,
						//'isLastTrip' : (legI + 1 == length)
						this_ : this_
					};
    			this_.module.webapp.indexApi.getTripStops(tripId, context, 
					function(data) {
						var stopsElem = $('#journey-details-leg-trip-stops' + this.tripId.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )),
							foundFirstLegStation = false;
	                    for(var i = 0, dataLength = data.length; i < data.length; i++) {
	                    	var stopId = data[i].id;
	                    	if (!foundFirstLegStation) {
	                    		if (stopId === this.leg.from.stopId) {
		                    		foundFirstLegStation = true;
		                    	}
	                    		continue;
	                    	}
	                    	if (stopId === this.leg.to.stopId) break;
	                        ich['otp-journeyresults-details-leg-stop']({
	                			'stopName' : data[i].name
	                			//'stopTime' : endTimeStr	                			
	            			})
	            			.data('stopId', data[i].id)
	            			.appendTo(stopsElem);
	                    }
	                    //stopsElem.data('legStartTime', this.leg.startTime);
	                    this.this_.module.webapp.indexApi.getTripStoptimes(tripId, 
	                    	{ divContainer : stopsElem, legStartDay : moment(this.leg.startTime).utcOffset('+06:30').startOf('day') }, 
	                    	function(data) {
	                    		var legStartDay = this.legStartDay;
	                    		$( this.divContainer.children() ).each(
                    	    		function(index, element) { 
                    	    			var stopId = $(element).data('stopId');
                    	    			for(var i = 0, dataLength = data.length; i < data.length; i++) {
                	                    	if (stopId === data[i].stopId) {
                	                    		ich['journey-details-leg-stop-time']({
                    	                			'arrival' : otp.util.Time.formatItinTime(
                    	                					legStartDay.clone().add( moment.duration( Number( data[i].scheduledArrival ), 's')).valueOf(), 
                    	                					otp.config.locale.time.time_format),
                    	                			'departure' : otp.util.Time.formatItinTime(
                    	                					legStartDay.clone().add( moment.duration( Number( data[i].scheduledDeparture ), 's')).valueOf(), 
                    	                					otp.config.locale.time.time_format)
                    	            			})
                    	            			.appendTo($(element).find('.journey-details-leg-stop-name-and-time'));
                	                    		break;
                	                    	}
                	                    }
                    	    		}
                    	    	);
	                    	}
	                    );
					}
				);

                $(this).removeClass('show').addClass('hide');
                $(this).parent().find('.triangle-up-img').removeClass('hide').addClass('show');
            });
    		details.find('.triangle-up-img').click(function(evt) {
    			var tripId = $(this).parent().data('tripId');
    			$('#journey-details-leg-trip-stops' + tripId.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" )).empty();
    			
                $(this).removeClass('show').addClass('hide');
                $(this).parent().find('.triangle-down-img').removeClass('hide').addClass('show');
            });
        	details.appendTo($( ".journey-details-element" )); //appendTo(insertionGoal);
    		if (l == itin.itinData.legs.length - 1) {
    			duration1st_currentLeg = Math.floor(otp.util.Time.calcDuration(itin.itinData.legs[0].startTime, leg.endTime));
    			ich['otp-journeyresults-details-stop']({
        			'stopName' : leg.to.name,
        			'stopTime' : endTimeStr,
        			'nextDay' : this.composeNextDaySentence(duration1st_currentLeg, null),
        			'showLeg' : false
    			}).appendTo($( ".journey-details-element" )); //appendTo(insertionGoal);
    		}
    	}
    },

    composeNextDaySentence : function (duration, zeroSentence) {
    	var sentence;
    	if (0 == duration) {
    		sentence = zeroSentence;
    	} else if (1 == duration) {
    		sentence = 'Next day';
    	} else {
    		sentence = duration;
    	}
        return sentence;
    },

    _onClickItinerary : function (event) {
    	var itin = $(this).data('itin');
        this.module.drawItinerary(itin);
        this.activeIndex = $(this).data('index');        
    },

    CLASS_NAME : "otp.newbie.TripWidgetAllInOne"
});
