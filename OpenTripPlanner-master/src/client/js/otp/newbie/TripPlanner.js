L.Control.TripPlanner = L.Control.AbstractControlPanel.extend({
    options: {
        collapsed: false,
        //position: 'bottomleft',
        autoZIndex: true
    },

    initialize: function (options) {
        L.Control.AbstractControlPanel.prototype.initialize.call(this, options);
    },

    /*onAdd: function (map) {
        L.Control.AbstractControlPanel.prototype.onAdd.call(this, map);
        //this._map = map;

        return this._container;
    },*/

    onRemove: function (map) {
        L.Control.AbstractControlPanel.prototype.onRemove.call(this, map);
    },

    _update: function () {
        if (!this._container) {
            return;
        }
    },

    _createUIElements: function () {
        var panels = ich['otp-journeyresults-panel']({
            'Plan Your Trip' : _tr("Plan Your Trip"),
            'Plan your journey' : this.options.title,
            'whenList' : this.options.whenList,
            'Return to Trip Planner': _tr("Return to Trip Planner")
        });
        panels.appendTo($(this._form));
        panels.find('.locSelector-reverseButton').click(this.options.onReverseButtonClick);
        panels.find('#clearStartEndLocationsBtn').click(this.options.onClearButtonClick);

        panels.find('#when')
            .on('change', { context :this }, this._onSelectOptionChanged)
            .on('change', this.options.onWhenSelectChanged);
        
        panels.find('#buttonGo').click(this.options.onBtnGoClick);
        panels.find('#buttonReturnToTripPlanner').click($.proxy(function() { this.selectSearchOptionsMode() }, this));

        $(this._form).find('#timepickerTripPlanner').timepicker({
            //minuteStep: 1,
            //template: 'modal',
            //appendWidgetTo: this._form,
            //showSeconds: true,
            showMeridian: false,
            autoCalcZIndex: false
            //defaultTime: false
        }).on('changeTime.timepicker', this.options.onTimePickerChanged);
        $(this._form).find('#datepickerTripPlannerDiv').datepicker({ 
            autoclose : true,
            //'container' 'todayBtn' zIndexOffset
            todayBtn : true,
            todayHighlight : true
        }).on('changeDate', this.options.onDatePickerChanged)
        .data('datepicker').setDate(new Date());
    },
    
    selectItinerariesMode: function () {
        $('#tripPlannerPanel').addClass('hide');
        $('#journeyResults').removeClass('hide');
    },
    
    selectSearchOptionsMode: function () {
    	$('#journeyResults').addClass('hide');
        $('#tripPlannerPanel').removeClass('hide');
    },

    _onSelectOptionChanged: function (event) {
        _this = event.data.context;
        _this._handlingClick = true;
       
        switch (this.value) {
        	case 'depart_by' :
        	case 'arrive_by' :
                $('.bootstrap-timepicker').removeClass('hide');
                $('#datepickerTripPlannerDiv').removeClass('hide');
        		break;
        	case 'leave_now' :
        	default :
                $('.bootstrap-timepicker').addClass('hide');
                $('#datepickerTripPlannerDiv').addClass('hide');
        		break;
        }

        _this._handlingClick = false;

        _this._refocusOnMap();
    },
 
    _onInputClick: function () {
        var i, input, obj,
            inputs = this._form.getElementsByTagName('input'),
            inputsLen = inputs.length;

        this._handlingClick = true;

        //

        this._handlingClick = false;

        this._refocusOnMap();
    },
    
    _fillSelectElement : function (selectElement, list, initOption) {
        list.forEach( function(item, arrIndex) {
            var selectOption = document.createElement('option');
            initOption(selectOption, item);
            selectElement.appendChild(selectOption);
        });
    }
});
