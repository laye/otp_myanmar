/* This program is free software: you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public License
   as published by the Free Software Foundation, either version 3 of
   the License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

otp.namespace("otp.locale");

/**
  * @class
  */
otp.locale.English = {

    config :
    {
        //Name of a language written in a language itself (Used in Frontend to
        //choose a language)
        name: 'English',
        //FALSE-imperial units are used
        //TRUE-Metric units are used
        metric : false, 
        //Name of localization file (*.po file) in src/client/i18n
        locale_short : "en",
        //Name of datepicker localization in
        //src/client/js/lib/jquery-ui/i18n (usually
        //same as locale_short)
        //this is index in $.datepicker.regional array
        //If file for your language doesn't exist download it from here
        //https://github.com/jquery/jquery-ui/tree/1-9-stable/ui/i18n
        //into src/client/js/lib/jquery-ui/i18n
        //and add it in index.html after other localizations
        //It will be used automatically when UI is switched to this locale
        datepicker_locale_short: "" //Doesn't use localization

    },

    /**
     * Info Widgets: a list of the non-module-specific "information widgets"
     * that can be accessed from the top bar of the client display. Expressed as
     * an array of objects, where each object has the following fields:
     * - content: <string> the HTML content of the widget
     * - [title]: <string> the title of the widget
     * - [cssClass]: <string> the name of a CSS class to apply to the widget.
     * If not specified, the default styling is used.
     */
    infoWidgets : [
            {
                title: 'About',
                content: ['<div style = "margin-left:20%;">',
                	'<p>“I travel not to go anywhere, but to go.</p>',
                	'<p>I travel for travel’s sake. The great affair is to move.”</p>',
                	'</div>',
                	'<div style = "margin-left:50%;"><p>– Robert Louis Stevenson</p></div>',
                	'<p>We want to help you plan your journeys using the most up to date data available and show you rail connections that you didn’t know exist before.',
                	'</p>',
                	'<p>We want to help you to discover new and interesting places that you can reach through the use of Myanmar’s national railways.',
                	'</p>',
                	'<br/>',
                	'<p>With this in mind, WayMyanmar is designed to be a full journey planner aimed at independent travellers as well as regular commuters in Myanmar.',
                	'</p>',
                	'<p>KEY FEATURES</p>',
                	'<ul style="list-style-type:square">',
                	'<li>Plan trips using rail services throughout the country.</li>',
                	'<li>Exact arrival and departure times of journeys are provided, as well as duration of the trip and number of intermediate stops along the way.</li>',
                	'<li>View the complete route of the trip on the map.</li>',
                	'<li>View the exact positions of the rail stations.</li>',
                	'</ul>'].join(''),
                //cssClass: 'otp-contactWidget',
            },
            {
                title: 'Contact Us',
                content: ['<div>',
                    '<p>You can reach us in several ways:</p>',
                  	'<a class="contact-us-circle contact-us-logo" href="mailto:contact@waymyanmar.com" title="contact@waymyanmar.com"><span>@</span></a>',
                  	'<a class="contact-us-twitter-logo contact-us-logo" href="https://twitter.com/WayMyanmar" title="@waymyanmar"></a>',
                  	'<a class="contact-us-fb-logo contact-us-logo" href="https://www.facebook.com/WayMyanmar/" title="www.facebook.com/WayMyanmar"></a>',
                  	'</div>'].join('')
            },
    ],


    time:
    {
        format         : "MMM Do YYYY, h:mma", //moment.js
        date_format    : "MM/DD/YYYY", //momentjs must be same as date_picker format which is by default: mm/dd/yy
        time_format    : "h:mma", //momentjs
        time_format_picker : "hh:mmtt", //http://trentrichardson.com/examples/timepicker/#tp-formatting
    },

    CLASS_NAME : "otp.locale.English"
};

