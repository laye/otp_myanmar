/**
 * 
 */

otp.namespace("otp.newbie");

otp.newbie.IntermediateStopsLayer =
    otp.Class(L.LayerGroup, {

    module : null,
    
    options: {
    	lowerZoomStops : 15,
        stoppingStationIcon : L.icon({
            iconUrl: resourcePath + 'images/marker-100pct.png',
            shadowUrl: null,
            iconSize: new L.Point(13, 23),
            iconAnchor: new L.Point(6, 23),
            popupAnchor: new L.Point(0, -10)
        }),
    	stopIcon : L.icon({
	        iconUrl: resourcePath + 'images/stop20.png',
	        shadowUrl: null,
	        iconSize: new L.Point(20, 20),
	        iconAnchor: new L.Point(10, 10),
	        popupAnchor: new L.Point(0, -5)
    	})
    },

    initialize : function(module, options) {
        var this_ = this;
        L.setOptions(this, options);
        L.LayerGroup.prototype.initialize.apply(this);
        this.module = module;

        this.stopsLookup = {};

        this.module.addLayer("stops", this);
        this.module.webapp.map.lmap.on('dragend zoomend', $.proxy(this.refresh, this));
        
        this.selectedTripStopsLayer = L.layerGroup();
        this.module.addLayer("selectedTripStops", this.selectedTripStopsLayer);
        this.selectedTripStopsSet = {};
    },

    setSelectedItinerary : function(itin) {
    	this.selectedTripStopsLayer.clearLayers();
    	this.selectedTripStopsSet = {};
    	this.selectedItin = itin;
    	this.selectedTrips = {};
    	if (null == itin) {    		
    		this.refresh();
    	} else {
    		for (var legI = 0, length = itin.itinData.legs.length; legI < length; legI++) {
    			var leg = itin.itinData.legs[legI];
    				tripId = leg.tripId;
    			this.selectedTrips[tripId] = { 'tripId' : tripId };
    			this.selectedTrips[tripId].from = leg.from;
    			this.selectedTrips[tripId].to = leg.to;
    			this.module.webapp.indexApi.getTripStops(tripId, 
    				{ 
    					'stopLayer' : this, 
    					'trip' : this.selectedTrips[tripId],
                        'routeShortName' : leg.routeShortName,
                        'agencyName' : leg.agencyName,
    					'isFirstTrip': 0 == legI,
    					'isLastTrip' : (legI + 1 == length)
    				}, 
    				function(data) {
                        var tripData = {
                            'routeShortName' : this.routeShortName,
                            'agencyName' : this.agencyName
                        };
	                    for(var i = 0, dataLength = data.length; i < data.length; i++) {
	                        var stopId = data[i].id;
	                        if (this.trip.from.stopIndex <= i && i <= this.trip.to.stopIndex) {
	                        	data[i].tripId = this.trip.tripId;
	                        	if (0 == i && this.isFirstTrip) {
	                        		data[i].start_destination_or_intermediate = 1; // start station
	                        	} else if ( (dataLength == i + 1) && this.isLastTrip) {
	                        		data[i].start_destination_or_intermediate = 2; // destination station
	                        	} else {
	                        		data[i].start_destination_or_intermediate = 0;
	                        	}
                                data[i].tripData = tripData;
		                        this.stopLayer.selectedTripStopsSet[stopId] = data[i];
	                        }
	                    }
	                    if (this.isLastTrip) {
	                    	this.stopLayer.updateStopsOfTrips();
		                    this.stopLayer.refresh();
	                    }
    				}
    			);
    		}
    	}    	
    },

    refresh : function() {
        this.clearLayers();
        var lmap = this.module.webapp.map.lmap;
        if(lmap.getZoom() >= this.options.lowerZoomStops) {
            this.module.webapp.indexApi.loadStopsInRectangle(null, lmap.getBounds(), this, function(data) {
                this.stopsLookup = {};
                for(var i = 0; i < data.length; i++) {
                    var agencyAndId = data[i].id;
                    this.stopsLookup[agencyAndId] = data[i];
                }
                this.updateStops();
            });
        }
    },

    updateStopsOfTrips : function() {
        var stops = _.values(this.selectedTripStopsSet);
        var this_ = this;

        for(var i=0; i<stops.length; i++) {

            var stop = stops[i];
            stop.lat = stop.lat || stop.stopLat;
            stop.lon = stop.lon || stop.stopLon;

            //var icon = new StopIcon20();

            m = L.marker([stop.lat, stop.lon], {
                icon : this_.options.stoppingStationIcon,
            });
            m._stop = stop;
            m._stopId = stop.id;
            m._tripId = stop.tripId;
            m._start_destination_or_intermediate = stop.start_destination_or_intermediate;
            m.addTo(this.selectedTripStopsLayer)
             .bindPopup('')
             .on('click', function() {
                var stopId = this._stopId;
                var stopIdArr = stopId.split(':');
                var marker = this;
                this_.module.webapp.indexApi.loadStopById(stopIdArr[0], stopIdArr[1], this_, function(detailedStop) {
                    marker.setPopupContent(this_.getPopupContentOfSelectedItineraryStop(detailedStop, marker));
                    //TODO: set Date
                    this.runTimesQuery(stopId, marker._tripId, Date.now(), marker._start_destination_or_intermediate);
                });
             });
        }
    },

    updateStops : function(stops) {
        var stops = _.values(this.stopsLookup);
        var this_ = this;

        for(var i=0; i<stops.length; i++) {

            var stop = stops[i];
            if ( this.selectedTripStopsSet.hasOwnProperty(stop.id) )
            	continue; // this stop was drawn by this.updateStopsOfTrip
            stop.lat = stop.lat || stop.stopLat;
            stop.lon = stop.lon || stop.stopLon;

            m = L.marker([stop.lat, stop.lon], {
                icon : this_.options.stopIcon,
            });
            m._stop = stop;
            m._stopId = stop.id;
            m.addTo(this)
             .bindPopup('')
             .on('click', function() {
                var stopId = this._stopId;
                var stopIdArr = stopId.split(':');
                var marker = this;
                this_.module.webapp.indexApi.loadStopById(stopIdArr[0], stopIdArr[1], this_, function(detailedStop) {
                    marker.setPopupContent(this_.getPopupContent(detailedStop));
                    //hide the "Routes Serving Stop:" section altogether
                    /*this_.module.webapp.indexApi.loadRoutesForStop(stopId, this_, function(data) {
                        _.each(data, function(route) {
                            ich['otp-stopsLayer-popupRoute'](route).appendTo($('.routeList'));
                        });
                    });*/
                });
             });
        }
    },

    /* Compose popup content for the selected itinerary's stop*/
    getPopupContentOfSelectedItineraryStop : function(stop, marker) {
        var this_ = this;

        var stop_viewer_trans = _tr('Stop Viewer');
        //TRANSLATORS: Plan Trip [From Stop| To Stop] Used in stoplayer popup
        var plan_trip_trans = _tr('Plan Trip');
        //TRANSLATORS: Plan Trip [From Stop| To Stop] Used in stoplayer popup
        var from_stop_trans = _tr('From Stop');
        //TRANSLATORS: Plan Trip [From Stop| To Stop] Used in stoplayer popup
        var to_stop_trans = _tr('To Stop');
        var routes_stop_trans = _tr('Routes Serving Stop');

        // TriMet-specific code
        if(stop.url && stop.url.indexOf("http://trimet.org") === 0) {
            var stopId = stop.id.split(':')[1];
            stop.titleLink = 'http://www.trimet.org/go/cgi-bin/cstops.pl?action=entry&resptype=U&lang=en&noCat=Landmark&Loc=' + stopId;
        }
        var context = _.clone(stop);
        context.agencyStopLinkText = otp.config.agencyStopLinkText || "Agency Stop URL";
        context.stop_viewer = stop_viewer_trans;
        context.routes_on_stop = routes_stop_trans;
        context.plan_trip = plan_trip_trans;
        context.from_stop = from_stop_trans;
        context.to_stop = to_stop_trans;
        context.tripData = marker._stop.tripData;
        
        var popupContent = ich['otp-intermediateStopsLayer-popup'](context);

        popupContent.find('.stopViewerLink').data('stop', stop).click(function() {
            var thisStop = $(this).data('stop');
            this_.module.stopViewerWidget.show();
            this_.module.stopViewerWidget.setActiveTime(moment().add("hours", -otp.config.timeOffset).unix()*1000);
            this_.module.stopViewerWidget.setStop(thisStop.id, thisStop.name);
            this_.module.stopViewerWidget.bringToFront();
        });

        popupContent.find('.planFromLink').data('stop', stop).click(function() {
            var thisStop = $(this).data('stop');
            this_.module.setStartPoint(new L.LatLng(thisStop.lat, thisStop.lon), false, thisStop.name);
            this_.module.webapp.map.lmap.closePopup();
        });

        popupContent.find('.planToLink').data('stop', stop).click(function() {
            var thisStop = $(this).data('stop');
            this_.module.setEndPoint(new L.LatLng(thisStop.lat, thisStop.lon), false, thisStop.name);
            this_.module.webapp.map.lmap.closePopup();
        });

        return popupContent.get(0);
    },
    
    /* Compose popup content for stop not linked to the selected itinerary*/
    getPopupContent : function(stop) {
        var this_ = this;

        var stop_viewer_trans = _tr('Stop Viewer');
        //TRANSLATORS: Plan Trip [From Stop| To Stop] Used in stoplayer popup
        var plan_trip_trans = _tr('Plan Trip');
        //TRANSLATORS: Plan Trip [From Stop| To Stop] Used in stoplayer popup
        var from_stop_trans = _tr('From Stop');
        //TRANSLATORS: Plan Trip [From Stop| To Stop] Used in stoplayer popup
        var to_stop_trans = _tr('To Stop');
        var routes_stop_trans = _tr('Routes Serving Stop');

        // TriMet-specific code
        if(stop.url && stop.url.indexOf("http://trimet.org") === 0) {
            var stopId = stop.id.split(':')[1];
            stop.titleLink = 'http://www.trimet.org/go/cgi-bin/cstops.pl?action=entry&resptype=U&lang=en&noCat=Landmark&Loc=' + stopId;
        }
        var context = _.clone(stop);
        context.agencyStopLinkText = otp.config.agencyStopLinkText || "Agency Stop URL";
        context.stop_viewer = stop_viewer_trans;
        context.routes_on_stop = routes_stop_trans;
        context.plan_trip = plan_trip_trans;
        context.from_stop = from_stop_trans;
        context.to_stop = to_stop_trans;
        var popupContent = ich['otp-stopsLayer-popup'](context);

        popupContent.find('.stopViewerLink').data('stop', stop).click(function() {
            var thisStop = $(this).data('stop');
            this_.module.stopViewerWidget.show();
            this_.module.stopViewerWidget.setActiveTime(moment().add("hours", -otp.config.timeOffset).unix()*1000);
            this_.module.stopViewerWidget.setStop(thisStop.id, thisStop.name);
            this_.module.stopViewerWidget.bringToFront();
        });

        popupContent.find('.planFromLink').data('stop', stop).click(function() {
            var thisStop = $(this).data('stop');
            this_.module.setStartPoint(new L.LatLng(thisStop.lat, thisStop.lon), false, thisStop.stopName);
            this_.module.webapp.map.lmap.closePopup();
        });

        popupContent.find('.planToLink').data('stop', stop).click(function() {
            var thisStop = $(this).data('stop');
            this_.module.setEndPoint(new L.LatLng(thisStop.lat, thisStop.lon), false, thisStop.stopName);
            this_.module.webapp.map.lmap.closePopup();
        });

        return popupContent.get(0);
    },
    
    runTimesQuery : function(stopId, tripId, date, start_destination_or_intermediate) {
        //var this_ = this;
        //var startTime = moment(this.datePicker.val(), otp.config.locale.time.date_format).add("hours", -otp.config.timeOffset).unix();        
        this.module.webapp.indexApi.runStopTimesQuery(stopId, date, this, function(data) {
        	for(var i=0; i<data.length; i++) {
        		var stopTime = data[i];
        		for(var timeNo = 0; timeNo < stopTime.times.length; timeNo++) {
        			var timeElem = stopTime.times[timeNo];
        			if (tripId === timeElem.tripId) {
        				if (1 == start_destination_or_intermediate) { // start stop
        					ich['otp-intermediateStopsLayer-popupOneTimeElement']({
        						'timeElementName' : _tr("Departure"),
        						'timeValue' : moment.utc(timeElem.realtimeArrival*1000).format(otp.config.locale.time.time_format)
        					}).appendTo($('.stopPopupFooter'));
        				} else if (2 == start_destination_or_intermediate) { // destination stop
        					ich['otp-intermediateStopsLayer-popupOneTimeElement']({
        						'timeElementName' : _tr("Arrival"),
        						'timeValue' : moment.utc(timeElem.realtimeDeparture*1000).format(otp.config.locale.time.time_format)
        					}).appendTo($('.stopPopupFooter'));
        				} else { //(0 == start_destination_or_intermediate) // intermediate stop        					
        					timeElem.formattedRealtimeArrival = moment.utc(timeElem.realtimeArrival*1000).format(otp.config.locale.time.time_format);
        					timeElem.formattedrealtimeDeparture = moment.utc(timeElem.realtimeDeparture*1000).format(otp.config.locale.time.time_format);
        					var context = _.clone(timeElem);
        					context.arrivalTranslated = _tr("Arrival");
        					context.departureTranslated = _tr("Departure");
        					ich['otp-intermediateStopsLayer-popupTime'](context).appendTo($('.stopPopupFooter'));
        				}
        			}
        		}
        	}
        });
    }
});